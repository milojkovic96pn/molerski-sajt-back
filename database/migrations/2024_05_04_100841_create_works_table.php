<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('works', function (Blueprint $table) {
              $table->id();
              $table->string('type');
              $table->string('surface');
              $table->string('town');
              $table->string('othersWorkPercent')->nullable();
              $table->string('material');
              $table->string('otherWorks')->nullable();
              $table->string('note')->nullable();
              $table->string('name');
              $table->string('email')->nullable();
              $table->string('phone');
              $table->string('address');
              $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('works');
    }
};
