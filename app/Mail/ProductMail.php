<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ProductMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $phone;
    public $text;
    public $photo;
    public $title;
    public $color;
    public $size;
    public $material;
    public $shape;

    public function __construct($name,$email,$phone,$text,$photo,$title,$color,$size,$shape,$material)
       {
           $this->name = $name;
           $this->email = $email;
           $this->phone = $phone;
           $this->text = $text;
           $this->photo = $photo;
           $this->title = $title;

           $this->color = $color;
           $this->size = $size;
           $this->material = $material;
           $this->shape = $shape;

       }

    public function build()
       {
           return $this->view('mail.product')
                       ->subject('Naročilo pri ' . $this->name)
                       ->attach(storage_path("app/{$this->photo}"));
       }
}
