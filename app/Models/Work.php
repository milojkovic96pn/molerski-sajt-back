<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'surface',
        'town',
        'othersWorkPercent',
        'material',
        'otherWorks',
        'note',
        'name',
        'email',
        'phone',
        'address',
    ];
}
