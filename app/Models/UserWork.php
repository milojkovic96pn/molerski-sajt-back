<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWork extends Model
{
    protected $table = 'user_work';

    protected $fillable = ['user_id', 'work_id'];
}
