<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\UserWork;

class UserController extends Controller
{
    public function register(Request $request)
    {
       $messages = [
            'email.unique' => 'Korisnik sa tom email adresom već postoji',
        ];

        // Validate incoming request
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6',
            'phone' => 'required|string',
            'address' => 'required|string',
        ], $messages);

        // Create the user
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone' => $request->phone,
            'role' => "client",
            'paidUser' => 0,
            'limit' => 0,
            'address' => $request->address,
        ]);

        // Return a response
        return response()->json(['user' => $user], 201);
    }
     public function login(Request $request)
        {
            // Validate the incoming request data
            $request->validate([
                'email' => 'required|email',
                'password' => 'required|string',
            ]);

            // Attempt to authenticate the user
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                // Authentication was successful
                  $user = Auth::user();
                  $token = $user->createToken('Personal Access Token')->accessToken;
                  return response()->json([
                      'token' => $token->token,
                      'user' => $user,
                  ], 200);
            } else {
                // Authentication failed
                return response()->json(['message' => 'Email ili lozinka nisu tačni.'], 401);
            }
        }
     public function logout(Request $request)
        {
           $token = $request->bearerToken(); // Retrieve the token from the request headers

             if (!$token) {
                 return response()->json(['error' => 'Unauthorized'], 401);
             }

             // Delete the token from the personal_access_tokens table
             DB::table('personal_access_tokens')->where('token', $token)->delete();

             return response()->json(['message' => 'Successfully logged out'], 200);
        }

        public function getClients()
        {
           return User::all();
        }

            public function markAsPaid(Request $request, $id)
            {
                $user = User::findOrFail($id);

                // Update the paidUser column to "1"
                $user->paidUser = 1;
                $user->save();

                return response()->json(['message' => 'User marked as paid'], 200);
            }

              public function markAsUnPaid(Request $request, $id)
                {
                    $user = User::findOrFail($id);

                    // Update the paidUser column to "1"
                    $user->paidUser = 0;
                    $user->save();

                    return response()->json(['message' => 'User marked as paid'], 200);
                }
         public function changeLimit(Request $request, $id)
            {
                $user = User::findOrFail($id);

                // Update the paidUser column to "1"
                $user->limit = $request->limit;
                $user->save();

                return response()->json(['message' => 'User updated limit'], 200);
            }


             public function storeLimit(Request $request)
                {
                    // Validate the incoming request
                    $request->validate([
                        'user_id' => 'required|exists:users,id',
                        'work_id' => 'required|exists:works,id',
                    ]);
                    $user = User::find($request->user_id);
                    $userWorkLimit = UserWork::where('user_id', $request->user_id)->count();;
                    $userLimit = $user->limit;
                    $userWorkExistence = UserWork::where('user_id', $request->user_id)
                                     ->where('work_id', $request->work_id)
                                     ->exists();

                     $workUserCount = UserWork::where('work_id', $request->work_id)->count();
                     if ($workUserCount >= 5 && !$userWorkLimit) {
                                return response()->json(['message' => 'Oglas je kontaktiran više od 5 puta. Molimo vas da pronadjite novi oglas'], 403);
                            }


                     if ($userWorkLimit >= $userLimit && !$userWorkExistence) {
                        return response()->json(['message' => 'Iskoristio si svoj limit, pretplati se na veći.'], 403);
                    }

                    // Check if the entry already exists
                    if (UserWork::where('user_id', $request->user_id)
                                ->where('work_id', $request->work_id)
                                ->exists()) {
                        return response()->json(['message' => 'Limit exists'], 201);
                    }

                    // Create a new entry
                    UserWork::create([
                        'user_id' => $request->user_id,
                        'work_id' => $request->work_id,
                    ]);

                    return response()->json(['message' => 'Entry created successfully'], 201);
                }
}
