<?php

namespace App\Http\Controllers;
use App\Models\Work;
use Illuminate\Http\Request;

class WorkController extends Controller
{
    // GET all works
       public function index(Request $request)
       {
         $token = $request->bearerToken();
         if($token) {
            return Work::all();
         } else {
           return Work::select('id', 'type', 'surface', 'town','othersWorkPercent', 'material','otherWorks','note','name','email','created_at')->get();
         }
       }

       // Store a new work
       public function store(Request $request)
       {
           $work = new Work();
           $work->fill($request->all());
           $work->save();

           return response()->json($work, 201);
       }
}
