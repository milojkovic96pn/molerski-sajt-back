<!DOCTYPE html>
<html>
<head>
    <title>Kontaktni podatki</title>
</head>
<body>
    <h2>Kontaktni obrazec</h2><br>
    <p><strong>Ime:</strong> {{ $name }}</p>
    <p><strong>Email:</strong> @if($email) {{ $email }} @else / @endif</p>
    <p><strong>Telefon:</strong> {{ $phone }}</p>
    <p><strong>Sporočilo:</strong> {{ $text }} </p>
</body>
</html>
