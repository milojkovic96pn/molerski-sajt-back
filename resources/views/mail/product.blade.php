<!DOCTYPE html>
<html>
<head>
    <title>Informacije o naročilu</title>
</head>
<body>
    <h2>Novo naročilo: <b>{{ $title }}</b></h2><br>
    <p><strong>Vrsta tiska:</strong> {{ $title }}</p>
    <p><strong>Ime:</strong> {{ $name }}</p>
    <p><strong>Email:</strong> @if($email) {{ $email }} @else / @endif</p>
    <p><strong>Telefon:</strong> {{ $phone }}</p>
    <p><strong>Sporočilo:</strong> @if( $text ) {{ $text }} @else / @endif</p>
    @if($color)
     <p><strong>Boja:</strong> {{ $color }} </p>
    @else  @endif
     @if($size)
         <p><strong>Velikost:</strong> {{ $size }} </p>
     @else  @endif
     @if($material)
         <p><strong>Material:</strong> {{ $material }} </p>
     @else  @endif
      @if($shape)
     <p><strong>Oblik:</strong> {{ $shape }} </p>
    @else  @endif
</body>
</html>
