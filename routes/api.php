<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WorkController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::post('/register', [UserController::class, 'register']);
Route::post('/login', [UserController::class, 'login']);
Route::post('/logout', [UserController::class, 'logout']);
Route::get('/clients', [UserController::class, 'getClients']);
Route::put('/clients/{id}/mark-as-paid', [UserController::class, 'markAsPaid']);
Route::put('/clients/{id}/mark-as-unpaid', [UserController::class, 'markAsUnPaid']);
Route::put('/clients/{id}/limit', [UserController::class, 'changeLimit']);

Route::get('/works', [WorkController::class, 'index']);
Route::post('/works', [WorkController::class, 'store']);

Route::post('/user-work', [UserController::class, 'storeLimit']);
